# Changelog

= 1.0.12 =
* 19 August 2020
* Fixing typos in the README

= 1.0.11 =
* 18 August 2020
* Fixing a bug with duplicate saving logic.  Updating the WordPress banner.

= 1.0.10 =
* 18 August 2020
* Fixing logical error in rewrite rules; some refactring also.

= 1.0.9 =
* 18 August 2020
* Fixing the README.txt.  AGAIN.

= 1.0.8 =
* 18 August 2020
* Fixing the README.txt so the plugin displays correctly on the WordPress plugin's site.

= 1.0.7 =
* 18 August 2020
* Attempting to fix the images so they will display on the WordPress plugins site.

= 1.0.6 =
* Removing Dependency on Permalink Manager Lite
* Fixed some issues with saving meta fields and only displaying video / audio
  if the user entered them.

= 1.0.5 =
* Adding nonce to form, fixing email sanitation, checking user capabilities.

= 1.0.4 =
* Fixed undefined variable

= 1.0.3 =
* 14 August 2020
* Adding Sanitization, Escaping, and Validation to form fields
* Adding namespace
* Fixing the RSS Feed refresh
* Refactoring
* Added linting / fixing tools with instructions in the README.md

= 1.0.2 =
* 12 August 2020
* Fixed type in admin field

= 1.0.1 =
* 12 August 2020
* Fixed podcast explicit value

= 1.0.0 =
* 2 August 2020
* Initial version
