# Simple Sermon Media & Podcast

Provides media and podcast support for video and audio sermons.

# Details

Note: This plugin currently only supports YouTube videos and MP3 files, but additional
formats could certainly be added.

This plugin provides a "Sermons" post type that can be used to share video and audio
sermons.  When a sermon post is viewed, the video and audio are displayed
in a user-friendly, tabbed format for "Video," "Audio," and "Download" (which
allows the download of MP3 sermons).

It also provides a podcast RSS feed with an options page for various settings
for the podcast.

This currently does exactly what *I* need it to do, but I am very much open to
suggestions and (especially) contributions by others who would like to expand it.

# How to use

You can install this plugin from WordPress directly.  However, if you are reading this,
you are looking at the source code repository.  If you want to try installing from
source, here's how:

1. Clone this repo
1. Create a zip file archive of the 'simple-sermon-media-podcasting' directory
1. Login to manage your WordPress site
1. From the WordPress Dashboard, click "Plugins."
1. Click "Add New" at the top of the page.
1. Click "Upload Plugin" at the top of the page
1. Click "Browse," and select the simple-sermon-media-podcasting.zip file
1. Click "Install Now"
1. Click "Activate"
1. Enter the settings to use for the media and podcast features.

## Bonus

1. If you also install the [BLB ScriptTagger](https://www.blueletterbible.org/webtools/BLB_ScriptTagger.cfm)
plugin, it can convert your Bible passages into hyperlinks that display that passage in a pop-up
window when you hover over them with your mouse.

# Development

## Docker

For local development, you can use the [Docker Composer](docker/docker-compose.yml) file in this repo. 

    cd docker/
    docker-compose up -d

You can get into bash on that container like this:

    docker exec -it docker_wordpress_1 /bin/bash

## Checking your code!

You can run some linting tests and repair tools.  Here are the commands I used to set that up locally.

    apt install php libapache2-mod-php php-mbstring php-xmlrpc php-soap php-gd php-xml php-cli php-zip
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
    sudo chmod +x /usr/local/bin/composer
    source ~/.bashrc
    touch composer.json 
    composer require --dev "squizlabs/php_codesniffer=*"
    composer require --dev friendsofphp/php-cs-fixer
    mkdir PHP_CodeSniffer_Sniffs
    git clone -b master https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards.git "$(pwd)/PHP_CodeSniffer_Sniffs/wpcs"
    ./vendor/bin/phpcs --config-set installed_paths "$(pwd)/PHP_CodeSniffer_Sniffs/wpcs"
    ./vendor/bin/phpcs --config-set default_standard WordPress
    ./vendor/bin/phpcs -s simple-sermon-media-podcasting/
    ./vendor/bin/phpcbf --config-set default_standard WordPress
    ./vendor/bin/phpcbf simple-sermon-media-podcasting/

## Administration

Adding the plugin to the WordPress SVN initially:

    mkdir svn
    svn co https://plugins.svn.wordpress.org/simple-sermon-media-podcast svn
    cp -rp simple-sermon-media-podcasting/* svn/trunk/
    cd svn
    svn add trunk/*
    svn ci -m 'Some commit message.'

Updating the Plugin in SVN:

    cp -rp simple-sermon-media-podcasting/* svn/trunk/
    cd svn
    svn ci -m 'Some commit message.'

Tagging in SVN:

    svn cp trunk tags/<version>
    svn ci -m "tagging version <version>"

# Links

Plugin on WordPress: https://wordpress.org/plugins/simple-sermon-media-podcast/
Plugin on trac: https://plugins.trac.wordpress.org/log/simple-sermon-media-podcast
Using SVN with WordPress: https://developer.wordpress.org/plugins/wordpress-org/how-to-use-subversion/
