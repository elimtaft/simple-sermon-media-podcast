#!/bin/bash

my_version=$(grep "VERSION" simple-sermon-media-podcasting/simple-sermon-media-podcasting.php | awk -F "'" '{print $4}')
plugin_dir="simple-sermon-media-podcasting"
archive="${plugin_dir}_v${my_version}.zip"
if [ -f "${archive}" ]; then
    rm "${archive}"
fi
zip -r "${archive}" "${plugin_dir}"
